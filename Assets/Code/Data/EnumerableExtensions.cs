﻿using System;
using System.Collections.Generic;

public static class EnumerableExtensions {
    public static T GetFirst<T>(this IEnumerable<T> list)  {
        foreach(var item in list) {
            return item;
        }
        return default(T);
    }
    public static int GetCount<T>(this IEnumerable<T> list)  {
        var count = 0;
        foreach(var item in list) {
            count++;
        }
        return count;
    }
    public static bool IsEmpty<T>(this IEnumerable<T> list)  {
        return list.GetCount() == 0;
    }
}

