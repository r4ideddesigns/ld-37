﻿using System;
using UnityEngine;

public class MoneyManager : MonoBehaviour {
    public static float cash = 1000;

    public static bool CanAfford(AbstractPlacedItem item) {
        return item.cost <= cash;
    }

    public static bool Buy(AbstractPlacedItem item) {
        if(CanAfford(item)) {
            cash -= item.cost;
            return true;
        }
        return false;
    }

    public static void Refund(AbstractPlacedItem item) {
        cash += item.cost;
    }
}

