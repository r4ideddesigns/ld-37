﻿using System;
using System.Collections;
using UnityEngine;


public class Product : MonoBehaviour {
    #region Data / Init
    public string typeName;
    public const float productCollisionRadius = .2f;
    [NonSerialized]
    public AbstractPlacedItem currentHolder;
    const float speed = .01f;
    float blockedSince = 0f; // used to create a priority queue
    bool forceBlocked = false; // used to enforce collisions across script calls.
    bool isBlocked;
    Location target {
        get {
            return currentHolder.outputLocations.GetFirst();
        }
    }

    void OnTriggerStay(Collider other) {
        if(!currentHolder.outputLocations.IsEmpty()) {
            if((transform.position - target.position).sqrMagnitude
                > (other.gameObject.transform.position - target.position).sqrMagnitude) {
                isBlocked = true;
            }
        }
    }
    #endregion

    protected void FixedUpdate() {
        Assert.IsTrue(currentHolder.IsPlaced);
        isBlocked = false;
        StartCoroutine(FixedUpdatedAfterCollisionStay());
    }

    IEnumerator FixedUpdatedAfterCollisionStay() {
        if(!currentHolder.outputLocations.IsEmpty()) {
            yield return new WaitForEndOfFrame();
            DoFixedUpdatedAfterCollisionStay();
        }
    }

    void DoFixedUpdatedAfterCollisionStay() {
        forceBlocked = false;

        var targetPosition = target.position;
        targetPosition.y = transform.position.y;
        var destinationPosition = Vector3.MoveTowards(transform.position, targetPosition, speed);

        if(isBlocked) {
            if(blockedSince <= 0) {
                blockedSince = Time.time;
            }
            return;
        }

        if(destinationPosition == targetPosition) {
            var targetItem = Grid.GetItem(target);
            if(targetItem != null) {
                currentHolder.products.Remove(this);
                var machine = targetItem as Machine;
                if(machine != null) {
                    if(machine.Consume(this)) {
                        Destroy(gameObject);
                    }
                    return;
                }
                targetItem.products.Add(this);
                currentHolder = targetItem;
            }
        }

        transform.position = destinationPosition;
        blockedSince = 0f;
    }

    #region Helpers
    //bool IsBlocked(Vector3 targetPosition) {
    //    // this got forced to be blocked by another moving product.
    //    // this prevents 2 products that would be too close from moving together,
    //    // by (ab)using the difference in position between the their FixedUpdate() calls.
    //    if (forceBlocked) {
    //        return true;
    //    }

    //    // machines can also have products, this prevents them moving after spawn.
    //    foreach(var belt in Grid.GetSurrounding<AbstractPlacedItem>(targetPosition, true)) {
    //        for(int i = 0; i < belt.products.Count; i++) {
    //            var product = belt.products[i];
    //            if(product != this) {
    //                var isFartherFromTarget = ((transform.position - targetPosition).sqrMagnitude > (product.transform.position - targetPosition).sqrMagnitude);
    //                // first, check if those two Products "really" collide, which would mean one HAS to stop if we want to prevent overlapping Products.
    //                if (CheckCollision(product, productCollisionRadius)) {
    //                    if (isFartherFromTarget) {
    //                        return true;
    //                    } else {
    //                        product.forceBlocked = true;
    //                    }
    //                }
    //                // second (wider) collision creates a priority queue, and forces products on joined belts to stop BEFORE they actually collide.
    //                if (CheckCollision(product, productCollisionRadius * 2f)) {
    //                    if (isFartherFromTarget) {
    //                        if (blockedSince <= 0 || (product.blockedSince > 0f && blockedSince >= product.blockedSince)) {
    //                            return true;
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    return false;
    //}

    //bool CheckCollision(Product other, float size) {
    //    float x0 = transform.position.x, y0 = transform.position.z;
    //    float x1 = other.transform.position.x, y1 = other.transform.position.z;
    //    return (x0 < x1 + size && x0 + size > x1 && y0 < y1 + size && y0 + size > y1);
    //}
    #endregion
}