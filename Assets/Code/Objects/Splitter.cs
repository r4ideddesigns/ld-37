﻿//using UnityEngine;
//using System.Collections.Generic;

//public class Splitter : AbstractPlacedItem
//{
//    protected override Vector3[] CreateBlocks ()
//    {
//        return new Vector3[] { Vector3.zero };
//    }

//    // input only from the south, output alternating west and east.
//    protected override Port[] CreateInputs ()
//    {
//        return new Port[] {
//            new Port(this, 0, 0, EnumDirection.South)
//        };
//    }

//    protected override Port[] CreateOutputs ()
//    {
//        return new Port[] {
//            new Port(this, 0, 0, EnumDirection.West),
//            new Port(this, 0, 0, EnumDirection.East)
//        };
//    }

//    protected override bool supportsMultipleOutputs
//    {
//        get {
//            return true;
//        }
//    }

//    int currentMainOutput = 0;
//    bool needToAdvance = false;
//    void FixedUpdate()
//    {
//        if (needToAdvance) {
//            needToAdvance = false;
//            print("advance splitter");

//            List<Port> connectedOutputs = new List<Port>(connectedOutputs.Length);
//            for (int i = 0; i < base.connectedOutputs.Length; ++i) {
//                if (base.connectedOutputs[i].output != null) {
//                    connectedOutputs.Add(base.connectedOutputs[i]);
//                }
//            }

//            if (connectedOutputs.Count > 0) {
//                currentMainOutput = (currentMainOutput + 1) % connectedOutputs.Count;
//            } else {
//                currentMainOutput = -1;
//            }
//        }
//    }

//    public override Port firstConnectedOutput
//    {
//        get {
//            needToAdvance = true;
//            return currentMainOutput >= 0 ? connectedOutputs[currentMainOutput] : null;
//        }
//    }
//}