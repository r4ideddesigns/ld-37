﻿using System;
using UnityEngine;


public class Machine : AbstractPlacedItem {
    #region Data / Init
    public string inputType;
    public int inputCountPerProduct;
    public int maxStorage = 10;
    int productsRecieved;
    public GameObject product;
    int ticksSinceLastProduct, ticksPerProduct = 60;
    Product lastProduct;
    Color productColor;

    protected override Vector3[] CreateBlocks ()
    {
        return new Vector3[] { Vector3.zero };
    }
    protected override EnumDirection[] inputDirections {
        get {
            return new[] { EnumDirection.North, EnumDirection.East, EnumDirection.West };
        }
    }
    protected override EnumDirection[] outputDirections {
        get {
            return new[] { EnumDirection.South };
        }
    }

    protected void Start() {
        Assert.IsTrue(inputType != null || inputCountPerProduct == 0);
        Assert.IsTrue(inputCountPerProduct >= 0);
        productColor = UnityEngine.Random.ColorHSV();
    }
    #endregion

    public void FixedUpdate() {
        if(!IsPlaced) {
            return;
        }
        if(productsRecieved >= inputCountPerProduct) {
            if(!connectedOutputs.IsEmpty()) {
                if(ticksSinceLastProduct == 0) {
                    // check collision before spawning
                    if(lastProduct == null || (lastProduct.transform.position - transform.position).magnitude > Product.productCollisionRadius) {
                        var productPosition = transform.position + forward.GetVector();
                        lastProduct = Instantiate(this.product, productPosition, Quaternion.identity).GetComponent<Product>();
                        lastProduct.currentHolder = this;
                        Assert.IsTrue(lastProduct != null);
                        lastProduct.GetComponentInChildren<Renderer>().material.color = productColor;
                        products.Add(lastProduct); // add to the queue so we can also detect products backing up during Product.IsBlocked
                        productsRecieved -= inputCountPerProduct;
                    }
                }
            }
            ticksSinceLastProduct++;
            if(ticksSinceLastProduct >= ticksPerProduct) {
                if(!connectedOutputs.IsEmpty()) {
                    ticksSinceLastProduct = 0;
                } else {
                    ticksSinceLastProduct--;
                }
            }
        }
    }

    internal bool Consume(Product product) {
        if(product.typeName == inputType) {
            if(productsRecieved <= maxStorage) {
                productsRecieved++;
                return true;
            }
        }
        return false;
    }
}

